//
//  ItemAttributes.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct ItemAttributes : Decodable {
    
    //MARK: Decodable
    
    fileprivate enum CodingKeys: String, CodingKey
    {
        case name
        case color
        case description = "desc"
        case preview
    }
    
    //MARK: Properties
    
    let name : String
    let color : Color?
    let description : String?
    let preview : String?
}
