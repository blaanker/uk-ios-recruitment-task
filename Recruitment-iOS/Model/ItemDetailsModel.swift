//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

protocol ItemModelDetailsProtocol : ItemModelProtocol {
    var description : String { get }
}

struct ItemDetailsModel: ItemModelDetailsProtocol, Decodable {
    
    //MARK: Decodable
    
    fileprivate enum CodingKeys: String, CodingKey
    {
        case id
        case attributes
    }
    
    //MARK: ItemModelDetailsProtocol - Properties
    
    let id: String
    let name: String
    let color: Color
    let description : String
    var preview: String
    
    //MARK: Decodable - Initialization
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        let itemAttributes = try container.decode(ItemAttributes.self, forKey: .attributes)
        self.name = itemAttributes.name
        self.color = itemAttributes.color ?? .black
        self.description = itemAttributes.description ?? ""
        self.preview = itemAttributes.preview ?? ""
    }
    
}
