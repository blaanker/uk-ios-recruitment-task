//
//  TableItemCellViewModel.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct TableItemCellViewModel : ItemCellViewModel {
    
    //MARK: ItemCellViewModelProtocol - Properties
    
    let backgroundColor : Color
    let title : String
    
    //MARK: Properties
    
    let preview : String
    
    //MARK: Initialization
    
    init(itemModel : ItemModelProtocol) {
        self.backgroundColor = itemModel.color
        self.title = itemModel.name
        self.preview = itemModel.preview
    }
}
