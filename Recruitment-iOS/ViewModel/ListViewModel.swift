//
//  ListViewModel.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol ListViewModelDelegate : class {
    func listViewModelDidDownloadItemModels<ItemViewModelType : ItemCellViewModel>(_ viewModel : ListViewModel<ItemViewModelType>)
}

class ListViewModel<ItemViewModelType : ItemCellViewModel> : ViewModel {
    
    //MARK: Properties
    
    private(set) var itemModels : [ItemModelProtocol] = [] {
        didSet {
            self.items = self.itemModels.map({ ItemViewModelType(itemModel: $0) })
        }
    }
    private(set) var items : [ItemViewModelType] = [] {
        didSet {
            self.delegate?.listViewModelDidDownloadItemModels(self)
        }
    }
    weak var delegate : ListViewModelDelegate?
    
    //MARK: Methods - Internal methods
    
    func detailsViewModel(forItemAt index : Int) -> DetailsViewModel? {
        guard index < self.itemModels.count else {
            return nil
        }
        return DetailsViewModel(itemModel: self.itemModels[index])
    }
    
    func update(with items : [ItemModelProtocol]) {
        self.itemModels = items
    }

}
