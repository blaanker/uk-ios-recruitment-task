//
//  DetailsViewModel.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol DetailsViewModelDelegate : class {
    func detailsViewModel(_ viewModel : DetailsViewModel, didDownloadItemDescription description : String)
}

class DetailsViewModel : ViewModel, NetworkingManagerDelegate {
    
    //MARK: Properties
    
    let title : String
    let color : Color
    private let itemModel : ItemModelProtocol
    private let networkingManager : NetworkingManager = NetworkingManager(jsonParser: JSONParser())
    
    weak var delegate : DetailsViewModelDelegate?
    
    //MARK: Initialization
    
    init(itemModel : ItemModelProtocol) {
        self.itemModel = itemModel
        self.title = itemModel.name.enumerated().map { $0.offset % 2 != 0 ? $0.element.lowercased() : $0.element.uppercased() }.joined()
        self.color = itemModel.color
        
        self.networkingManager.delegate = self
    }
    
    //MARK: Methods - Internal methods
    
    func downloadItemDetails() {
        self.networkingManager.downloadItemWithID(self.itemModel.id)
    }
    
    //MARK: NetworkingManagerDelegate
    
    func downloadedItems(_ items : [ItemModel]) {
        
    }
    
    func downloadedItemDetails(_ itemDetails : ItemDetailsModel) {
        self.delegate?.detailsViewModel(self, didDownloadItemDescription: itemDetails.description)
    }
    
}
