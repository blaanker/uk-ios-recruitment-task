//
//  TabBarViewModel.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol TabBarViewModelDelegate : class {
    func tabBarViewModel(_ viewModel : TabBarViewModel, didDownloadItems items : [ItemModel])
}

class TabBarViewModel : ViewModel, NetworkingManagerDelegate {
    
    //MARK: Properties
    
    private let networkingManager : NetworkingManager = NetworkingManager(jsonParser: JSONParser())
    weak var delegate : TabBarViewModelDelegate?
    private(set) var items : [ItemModel] = [] {
        didSet {
            self.delegate?.tabBarViewModel(self, didDownloadItems: items)
        }
    }
    
    //MARK: Initialization
    
    init() {
        self.networkingManager.delegate = self
    }
    
    //MARK: Methods - Internal methods
    
    func downloadItems() {
        self.networkingManager.downloadItems()
    }
    
    //MARK: NetworkingManagerDelegate
    
    func downloadedItems(_ items: [ItemModel]) {
        self.items = items
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel) {
        
    }
    
}
