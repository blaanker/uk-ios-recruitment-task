//
//  ItemCellViewModel.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct CollectionItemCellViewModel : ItemCellViewModel {
    
    //MARK: ItemCellViewModelProtocol - Properties
    
    let backgroundColor : Color
    let title : String
    
    //MARK: Initialization
    
    init(itemModel : ItemModelProtocol) {
        self.backgroundColor = itemModel.color
        self.title = itemModel.name
    }
    
}


