//
//  ItemCellViewModelProtocol.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol ItemCellViewModel : ViewModel {
    var backgroundColor : Color { get }
    var title : String { get }
    
    init(itemModel : ItemModelProtocol)
}
