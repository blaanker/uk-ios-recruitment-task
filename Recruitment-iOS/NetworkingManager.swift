//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

protocol NetworkingManagerDelegate : class {
    func downloadedItems(_ items : [ItemModel])
    func downloadedItemDetails(_ itemDetails : ItemDetailsModel)
}

class NetworkingManager {
    
    //MARK: Properties
    
    private let jsonParser : JSONParserProtocol
    weak var delegate : NetworkingManagerDelegate?
    let timeout : TimeInterval = 2.0
    
    //MARK: Initialization
    
    init(jsonParser : JSONParserProtocol) {
        self.jsonParser = jsonParser
    }
    
    //MARK: Methods - Internal methods
    
    func downloadItems() {
        self.request(filename: "Items.json") { [weak self] dictionary in
            guard let data = dictionary["data"], let items : [ItemModel] = self!.response(from: data) else {
                return
            }
            self?.delegate?.downloadedItems(items)
        }
    }
    
    func downloadItemWithID(_ id:String) {
        let filename = "Item\(id).json"
        self.request(filename: filename) { [weak self] dictionary in
            guard let data = dictionary["data"], let itemDetails : ItemDetailsModel = self?.response(from: data) else {
                return
            }
            self?.delegate?.downloadedItemDetails(itemDetails)
        }
    }
    
    //MARK: Methods - Private methods
    
    private func response<T : Decodable>(from json : AnyObject) -> T? {
        guard let data = try? JSONSerialization.data(withJSONObject: json, options: []) else {
            return nil
        }
        return try? JSONDecoder().decode(T.self, from: data)
    }
    
    private func request(filename : String, completionBlock : @escaping (Dictionary<String, AnyObject>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + self.timeout) {
            if let dictionary = self.jsonParser.jsonFromFilename(filename) {
                completionBlock(dictionary)
            } else {
                completionBlock([:])
            }
        }
    }
    
}
