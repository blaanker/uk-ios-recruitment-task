//
//  ViewLoader.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ViewLoader<ViewControllerType : UIViewController & View>  {
    
    //MARK: Methods
    
    class func load(withViewModel viewModel : ViewControllerType.ViewModelType) -> ViewControllerType? {
        
        guard let name = String(describing: type(of: ViewControllerType.self)).components(separatedBy: ".").first else {
            return nil
        }
        
        let storyboard = UIStoryboard.main
        guard var viewController = storyboard.instantiateViewController(withIdentifier: name) as? ViewControllerType else {
            return nil
        }
        
        viewController.viewModel = viewModel
        return viewController
    }
    
}
