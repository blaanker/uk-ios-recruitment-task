//
//  View.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol View {
    associatedtype ViewModelType : ViewModel
    var viewModel : ViewModelType? { get set }
}
