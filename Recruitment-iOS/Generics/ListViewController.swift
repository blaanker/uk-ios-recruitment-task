//
//  ListViewController.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol ListViewController : View where ViewModelType == ListViewModel<ItemViewModelType> {
    associatedtype ItemViewModelType : ItemCellViewModel
    func openDetails(forItemAt indexPath : IndexPath)
}
