//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, View, DetailsViewModelDelegate {
    
    //MARK: IBOutlets
    
    @IBOutlet private weak var textView: UITextView!
    
    //MARK: Properties
    
    typealias ViewModelType = DetailsViewModel
    var viewModel : DetailsViewModel?
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.viewModel?.title
        self.view.backgroundColor = self.viewModel?.color.representation
        self.viewModel?.delegate = self
        self.viewModel?.downloadItemDetails()
    }
    
    //MARK: DetailsViewModelDelegate
    
    func detailsViewModel(_ viewModel: DetailsViewModel, didDownloadItemDescription description: String) {
        DispatchQueue.main.async {
            self.textView.text = description
        }
    }

}
