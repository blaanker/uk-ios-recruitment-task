//
//  ItemCollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell, View, ItemViewCellProtocol {

    //MARK: IBOutlets
    
    @IBOutlet private(set) weak var titleLabel: UILabel!
    
    //MARK: View - Properties
    
    typealias ViewModelType = CollectionItemCellViewModel
    var viewModel: CollectionItemCellViewModel? {
        didSet {
            self.update(with: self.viewModel)
        }
    }


}
