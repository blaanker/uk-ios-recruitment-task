//
//  ItemTableViewCell.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell, View, ItemViewCellProtocol {

    //MARK: IBOutlets
    
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    //MARK: View - Properties
    
    typealias ViewModelType = TableItemCellViewModel
    var viewModel: TableItemCellViewModel? {
        didSet {
            self.update(with: self.viewModel)
            self.descriptionLabel.text = self.viewModel?.preview
        }
    }
}
