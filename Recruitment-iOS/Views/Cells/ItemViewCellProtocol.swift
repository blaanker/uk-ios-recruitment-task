//
//  ItemViewCellProtocol.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemViewCellProtocol : class {
    
    var titleLabel : UILabel! { get }
    var backgroundColor : UIColor? { get set }
    
    func update(with viewModel : ItemCellViewModel?)
}

extension ItemViewCellProtocol {
    
    func update(with viewModel : ItemCellViewModel?) {
        guard let viewModel = viewModel else {
            return
        }
        self.titleLabel.text = viewModel.title
        self.backgroundColor = viewModel.backgroundColor.representation
    }
    
}
