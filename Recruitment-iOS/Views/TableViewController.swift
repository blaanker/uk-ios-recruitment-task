//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, ListViewController, ListViewModelDelegate {
    
    typealias ItemViewModelType = TableItemCellViewModel
    
    //MARK: Properties
    
    typealias ViewModelType = ListViewModel
    var viewModel : ListViewModel<TableItemCellViewModel>?
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(ItemTableViewCell.self)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 66
        self.viewModel?.delegate = self
    }
    
    //MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.items.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forClass: ItemTableViewCell.self, for: indexPath)
        
        guard let viewModel = self.viewModel?.items[indexPath.row] else {
            return cell
        }
        cell.viewModel = viewModel
        return cell
    }
    
    //MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openDetails(forItemAt: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: ListViewModelDelegate
    
    func listViewModelDidDownloadItemModels<ItemViewModelType>(_ viewModel: ListViewModel<ItemViewModelType>) where ItemViewModelType : ItemCellViewModel {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}
