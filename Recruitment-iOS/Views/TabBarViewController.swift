//
//  TabBarViewController.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, View, TabBarViewModelDelegate {
    
    //MARK: View - Properties
    
    typealias ViewModelType = TabBarViewModel
    var viewModel: TabBarViewModel?
    
    //MARK: Initialization
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        guard let viewControllers = self.viewControllers else {
            return
        }
        
        for viewControler in viewControllers {
            switch viewControler {
            case let controler as TableViewController:
                controler.viewModel = ListViewModel()
            case let controler as CollectionViewController:
                controler.viewModel = ListViewModel()
            default:
                break;
            }
        }
    }
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = TabBarViewModel()
        self.viewModel?.delegate = self
        self.viewModel?.downloadItems()
    }
    
    //MARK: TabBarViewModelDelegate
    
    func tabBarViewModel(_ viewModel: TabBarViewModel, didDownloadItems items: [ItemModel]) {
        guard let viewControllers = self.viewControllers else {
            return
        }
        
        for viewControler in viewControllers {
            switch viewControler {
                case let controler as TableViewController:
                    controler.viewModel?.update(with: items)
                case let controler as CollectionViewController:
                    controler.viewModel?.update(with: items)
                default:
                    break;
            }
        }
    }


}
