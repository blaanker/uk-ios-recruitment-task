//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewController: UICollectionViewController, ListViewController, ListViewModelDelegate, UICollectionViewDelegateFlowLayout {
    
    
    typealias ItemViewModelType = CollectionItemCellViewModel
    

    //MARK: View - Properties
    
    typealias ViewModelType = ListViewModel
    var viewModel: ListViewModel<ItemViewModelType>?
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(ItemCollectionViewCell.self)
        self.viewModel?.delegate = self
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.items.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forClass: ItemCollectionViewCell.self, for: indexPath)
        
        guard let viewModel = self.viewModel?.items[indexPath.row] else {
            return cell
        }
        
        cell.viewModel = viewModel
        return cell
    }
    
    //MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.openDetails(forItemAt: indexPath)
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let maxWidth = self.view.bounds.width / 2.0
        return CGSize(width: maxWidth, height: maxWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    //MARK: ListViewModelDelegate
    
    func listViewModelDidDownloadItemModels<ItemViewModelType>(_ viewModel: ListViewModel<ItemViewModelType>) where ItemViewModelType : ItemCellViewModel {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

}
