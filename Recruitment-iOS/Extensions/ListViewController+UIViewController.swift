//
//  ListViewController+UIViewController.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension ListViewController where Self : UIViewController {
    
    func openDetails(forItemAt indexPath : IndexPath) {
        
        guard let detailsViewModel = self.viewModel?.detailsViewModel(forItemAt: indexPath.row), let detailsViewController = ViewLoader<DetailsViewController>.load(withViewModel: detailsViewModel) else {
            return
        }
        
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
}
