//
//  UITableViewCell+Reuse.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register(_ cellClass: Swift.AnyClass?) {
        if let cellClass = cellClass {
            let reuseIdentifier = String(describing: type(of: cellClass)).components(separatedBy: ".").first!
            let nib = UINib(nibName: reuseIdentifier, bundle: nil)
            self.register(nib, forCellReuseIdentifier: reuseIdentifier)
        }
        
    }
    
    func dequeueReusableCell<Type : UITableViewCell>(forClass cellClass : Type.Type, for indexPath : IndexPath) -> Type {
        let name = String(describing: type(of: cellClass)).components(separatedBy: ".").first!
        return self.dequeueReusableCell(withIdentifier: name, for: indexPath) as! Type
        
    }
    
}
