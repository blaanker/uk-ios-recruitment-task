//
//  UIStoryboard+Instantiate.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static var main : UIStoryboard {
        get {
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
}
