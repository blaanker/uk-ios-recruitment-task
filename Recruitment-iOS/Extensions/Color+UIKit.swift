//
//  Color+UIKit.swift
//  Recruitment-iOS
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension Color {
    var representation : UIColor {
        get {
            switch self {
            case .red:
                return .red
            case .green:
                return .green
            case .blue:
                return .blue
            case .yellow:
                return .yellow
            case .purple:
                return .purple
            default:
                return .black
            }
        }
    }
}
