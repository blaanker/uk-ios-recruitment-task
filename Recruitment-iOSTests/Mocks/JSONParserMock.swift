//
//  JSONParserMock.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation


class JSONParserMock : JSONParserProtocol {
    
    var json : [String : AnyObject]?
    
    func jsonFromFilename(_ filename: String) -> [String : AnyObject]? {
        return self.json
    }
    
}
