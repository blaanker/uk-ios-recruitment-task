//
//  ItemModelMock.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct ItemModelMock : ItemModelProtocol {
    var id : String
    var name : String
    var color : Color
    var preview: String
    
    init(id : String, name : String, color : Color) {
        self.id = id
        self.name = name
        self.color = color
        self.preview = ""
    }
}
