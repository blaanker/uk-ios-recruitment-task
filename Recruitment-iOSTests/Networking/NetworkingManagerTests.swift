//
//  NetworkingManagerTests.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

class NetworkingManagerDelegateMock : NetworkingManagerDelegate {
    
    //MARK: Properties
    
    let excpectation : XCTestExpectation
    
    private(set) var items : [ItemModel]?
    private(set) var itemDetails : ItemDetailsModel?
    
    //MARK: Initialization
    
    init(excpectation : XCTestExpectation) {
        self.excpectation = excpectation
    }
    
    //MARK: NetworkingManagerDelegate
    
    func downloadedItems(_ items: [ItemModel]) {
        self.items = items
        self.excpectation.fulfill()
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel) {
        self.itemDetails = itemDetails
        self.excpectation.fulfill()
    }
}

class NetworkingManagerTests: XCTestCase {
    
    private var parser : JSONParserMock!
    private var networkingManager : NetworkingManager!

    override func setUp() {
        super.setUp()
        self.parser = JSONParserMock()
        self.networkingManager = NetworkingManager(jsonParser: self.parser)
    }

    override func tearDown() {
        self.networkingManager = nil
        self.parser = nil
        super.tearDown()
    }

    func testDownloadItems() {
        self.parser.json = [
            "data" : [
                [
                    "id" : "2",
                    "type" : "Items",
                    "attributes" : [
                        "name" : "Item2",
                        "preview" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                        "color" : "Red"
                    ]
                ],
                [
                    "id" : "1",
                    "type" : "Items",
                    "attributes" : [
                        "name" : "Item1",
                        "preview" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dictum eu velit vel ultrices. Ut vulputate scelerisque erat, ut mollis nibh convallis feugiat.",
                        "color" : "Green"
                    ]
                ]
            ]
        ] as [String : AnyObject]
        
        
        let excpectation = XCTestExpectation(description: "testDownloadItems")
        let delegate = NetworkingManagerDelegateMock(excpectation: excpectation)
        self.networkingManager.delegate = delegate
        self.networkingManager.downloadItems()
        
        wait(for: [excpectation], timeout: self.networkingManager.timeout)
        XCTAssertNotNil(delegate.items)
        XCTAssertEqual(delegate.items?.count, 2)
    }
    
    func testDownloadItemDetails() {
        self.parser.json = [
            "data" : [
                "id" : "1",
                "type" : "ItemDetails",
                "attributes" : [
                    "name" : "Item1",
                    "color" : "Green",
                    "desc" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus felis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam dignissim facilisis quam et malesuada. Sed imperdiet ipsum ut elit porttitor gravida. Nulla a lectus lorem. Proin sed volutpat risus. Aenean malesuada, nisl id finibus pellentesque, nunc felis pulvinar tellus, in commodo nisl urna et leo. Donec ac ante tortor. Pellentesque consequat tellus nec pellentesque euismod. Mauris euismod, leo auctor aliquet lacinia, dui est pulvinar nulla, fermentum dapibus felis leo nec est. Fusce ultrices, risus at ullamcorper cursus, leo sapien mattis arcu, vitae facilisis massa lacus et libero. Etiam sollicitudin augue porttitor tristique convallis. Aenean imperdiet, tortor dictum tristique ullamcorper, nulla nulla convallis tellus, sit amet luctus purus augue ac leo."
                ]
            ]
        ] as [String : AnyObject]
        
        
        let excpectation = XCTestExpectation(description: "testDownloadItemDetails")
        let delegate = NetworkingManagerDelegateMock(excpectation: excpectation)
        self.networkingManager.delegate = delegate
        self.networkingManager.downloadItemWithID("1")
        
        wait(for: [excpectation], timeout: self.networkingManager.timeout)
        XCTAssertNotNil(delegate.itemDetails)
        XCTAssertEqual(delegate.itemDetails?.id, "1")
        XCTAssertEqual(delegate.itemDetails?.name, "Item1")
    }

}
