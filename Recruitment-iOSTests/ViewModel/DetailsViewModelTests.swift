//
//  DetailsViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 14/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

class DetailsViewModelTests: XCTestCase {

    func testTitle() {
        let itemModel = ItemModelMock(id: "1", name: "Item1", color: .red)
        let viewModel = DetailsViewModel(itemModel: itemModel)
        XCTAssertEqual(viewModel.title, "ItEm1")
    }

}
