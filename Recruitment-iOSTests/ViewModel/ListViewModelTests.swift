//
//  ListViewModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 15/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

struct ItemCellViewModelMock : ItemCellViewModel {
    
    var backgroundColor: Color
    var title: String
    
    init(itemModel: ItemModelProtocol) {
        self.backgroundColor = itemModel.color
        self.title = itemModel.name
    }

}

class ListViewModelDelegateMock : ListViewModelDelegate {
    
    let excpectation : XCTestExpectation
    
    init(excpectation : XCTestExpectation) {
        self.excpectation = excpectation
    }
    
    func listViewModelDidDownloadItemModels<ItemViewModelType>(_ viewModel: ListViewModel<ItemViewModelType>) where ItemViewModelType : ItemCellViewModel {
        self.excpectation.fulfill()
    }
}

class ListViewModelTests : XCTestCase {
    
    func testListEmpty() {
        let viewModel = ListViewModel<ItemCellViewModelMock>()
        XCTAssertTrue(viewModel.items.isEmpty)
        XCTAssertTrue(viewModel.itemModels.isEmpty)
    }
    
    func testListUpdate() {
        let viewModel = ListViewModel<ItemCellViewModelMock>()
        
        let item1 = ItemModelMock(id: "1", name: "Item1", color: .red)
        let item2 = ItemModelMock(id: "2", name: "Item2", color: .purple)
        let items = [item1, item2]
        viewModel.update(with: items)
        
        XCTAssertEqual(viewModel.itemModels.count, viewModel.itemModels.count)
        XCTAssertEqual(viewModel.items.count, viewModel.itemModels.count)
    }
    
    func testListUpdateDelegateCalled() {
        let viewModel = ListViewModel<ItemCellViewModelMock>()
        let excpectation = XCTestExpectation(description: "testListUpdateDelegateCalled")
        let delegate = ListViewModelDelegateMock(excpectation: excpectation)
        viewModel.delegate = delegate
        let item1 = ItemModelMock(id: "1", name: "Item1", color: .red)
        let item2 = ItemModelMock(id: "2", name: "Item2", color: .purple)
        let items = [item1, item2]
        viewModel.update(with: items)
        wait(for: [excpectation], timeout: 1.0)
    }
    
    func testDetailsViewModelNil() {
        let viewModel = ListViewModel<ItemCellViewModelMock>()
        let item1 = ItemModelMock(id: "1", name: "Item1", color: .red)
        let item2 = ItemModelMock(id: "2", name: "Item2", color: .purple)
        let items = [item1, item2]
        viewModel.update(with: items)
        let details = viewModel.detailsViewModel(forItemAt: 20)
        XCTAssertNil(details)
    }
    
    func testDetailsViewModelNotNil() {
        let viewModel = ListViewModel<ItemCellViewModelMock>()
        let item1 = ItemModelMock(id: "1", name: "Item1", color: .red)
        let item2 = ItemModelMock(id: "2", name: "Item2", color: .purple)
        let items = [item1, item2]
        viewModel.update(with: items)
        let details = viewModel.detailsViewModel(forItemAt: 1)
        XCTAssertNotNil(details)
    }
    
}
