//
//  ItemModelTests.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

class ItemModelTests : XCTestCase {
    
    func testItemModel() {
        let itemModel : ItemModel = self.response(from: ["id" : "1",
                                                        "attributes" : [
                                                            "name" : "name1",
                                                            "color" : "Red",
                                                            "preview" : "Preview tests"]
                                                        ])
        XCTAssertEqual(itemModel.name, "name1")
        XCTAssertEqual(itemModel.color, .red)
        XCTAssertEqual(itemModel.preview, "Preview tests")
    }
    
    private func itemModel(withDictionary dictionary : [AnyHashable : Any]) -> ItemModel? {
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: []) else {
            return nil
        }
        return try? JSONDecoder().decode(ItemModel.self, from: data)
    }
}
