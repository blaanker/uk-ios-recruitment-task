//
//  ItemAttributesTests.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

class ItemAttributesTests : XCTestCase {
    
    func testItemAttributesName() {
        let itemAttributes : ItemAttributes = self.response(from: ["name" : "name1"])
        XCTAssertEqual(itemAttributes.name, "name1")
    }
    
    func testItemAttributesPreview() {
        let itemAttributes : ItemAttributes = self.response(from: ["name" : "name1", "preview" : "Item preview"])
        XCTAssertEqual(itemAttributes.preview, "Item preview")
    }
    
    func testItemAttributesDescription() {
        let itemAttributes : ItemAttributes = self.response(from: ["name" : "name1", "desc" : "Item description"])
        XCTAssertEqual(itemAttributes.description, "Item description")
    }
    
    func testItemAttributesColor() {
        let itemAttributes : ItemAttributes = self.response(from: ["name" : "name1", "color" : "Red"])
        XCTAssertEqual(itemAttributes.color, .red)
    }
    
}
