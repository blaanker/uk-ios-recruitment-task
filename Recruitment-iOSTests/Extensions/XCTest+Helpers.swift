//
//  XCTest+Helpers.swift
//  Recruitment-iOSTests
//
//  Created by Oskar Korzonek on 16/07/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest

extension XCTestCase {
    
    func response<T : Decodable>(from json : [AnyHashable : Any]) -> T {
        let data = try! JSONSerialization.data(withJSONObject: json, options: [])
        return try! JSONDecoder().decode(T.self, from: data)
    }
    
}
